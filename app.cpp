#include <string>

#if 0
namespace log4tango
{

struct Logger
{
    void info (const std::string& file, int line, const char* string_format, ...);
    void info (const std::string& file, int line, const std::string& message);
};

void Logger::info(const std::string& file, int line, const char* string_format, ...)
{
}

void Logger::info(const std::string& file, int line, const std::string& message)
{
}

struct Level 
{
  typedef enum {
    OFF         =  100,
    FATAL       =  200,
    ERROR       =  300, 
    WARN        =  400,
    INFO        =  500,
    DEBUG       =  600
  } LevelLevel;

  typedef int Value;
};

}

typedef void (log4tango::Logger::*StringOnlyLogSignature)(const std::string&);
typedef void (log4tango::Logger::*StringAndLocationLogSignature)(const std::string&, int, const std::string&);

typedef void (log4tango::Logger::*StringOnlyWithLevelLogSignature)(
    log4tango::Level::Value, const std::string&);

typedef void (log4tango::Logger::*StringAndLocationWithLevelLogSignature)(
    const std::string&, int, log4tango::Level::Value, const std::string&);

template <StringOnlyLogSignature ptr>
static void call_logger(log4tango::Logger& logger,
    const std::string & /*file*/, int /*line*/, const std::string& msg)
{
    return (logger.*ptr)(msg);
}

template <StringAndLocationLogSignature ptr>
static void call_logger(log4tango::Logger& logger,
    const std::string &file, int line, const std::string& msg)
{
    return (logger.*ptr)(file, line, msg);
}

template <StringOnlyWithLevelLogSignature ptr>
static void call_logger(log4tango::Logger& logger,
    const std::string & /*file*/, int /*line*/,
    log4tango::Level::Value level, const std::string& msg)
{
    return (logger.*ptr)(level, msg);
}

template <StringAndLocationWithLevelLogSignature ptr>
static void call_logger(log4tango::Logger& logger,
    const std::string &file, int line,
    log4tango::Level::Value level, const std::string& msg)
{
    return (logger.*ptr)(file, line, level, msg);
}

template <typename F>
void def(const char*, F)
{
}

#endif


struct A {};
struct B {};

void log(A) {}; // new signature
// void log(B) {}; // old signature, only one is present

template <void (*ptr)(A)>
void call_logger() {}

template <void (*ptr)(B)>
void call_logger() {}

void test()
{
    void (*ptr)() = &call_logger<&log>;
}

int main(int, char**)
{
    test();

    // StringAndLocationLogSignature ptr = &log4tango::Logger::info;

    // def("__info", &log4tango::Logger::info);

    // def("__info", &call_logger<&log4tango::Logger::info>);
    // def("__info", &call_logger<(StringAndLocationLogSignature)&log4tango::Logger::info>);
    return 0;
}
